'use strict';

/**
 * Transform promise into object { data, err }
 * @param {promise} promise
 * @return {object}
 */
function to(promise) {
  return mapPromiseToObject(promise);
}

async function mapPromiseToObject(promise) {
  try {
    const data = await promise;
    return {
      data: data,
    };
  } catch(err) {
    return {
      err: err,
    };
  }
}

module.exports = to;
